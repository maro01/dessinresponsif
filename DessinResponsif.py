import cv2
import numpy as np
import time
import os
import HandTrackingModule as htm

################ I- PARAMETRES


largeurWebCam=1280 #Largeur de la webCam en pixeles
hauteurWebCam=720  #Hauteur de la webCam en pixeles
brushThickness = 15   #Variable d'épaisseur du dessin
eraserThickness = 100  #Variable d'épaisseur de la gomme
curr_tool = "Courbe"  #Mode de dessin par défaut
curr_color = "Red"   #Couleur de dessin par defaut
folderPath = "Header" #Chemin des images de barre de tâches
xp,yp = 0,0           #coordonnées de point précedent par rapport au mouvement de dessin
Y_barreTache = 125    #Point de debut de la barre de tâche en axe des ordonnées en pixeles
debutEspaceOutils = 625 #Point de debut de la barre d'outils en axes des abcisses en pixeles
finEspaceOutils = 1280  #Point de fin de la barre d'outils en axes des abcisses en pixeles
debutEspaceCouleurs=9   #Point de debut de la barre de couleurs en axes des abcisses en pixeles
finEspaceCouleurs=552   #Point de fin de la barre de couleurs en axes des abcisses en pixeles
Compteur_NbrCaptures=0  #Compteur de dessins capturés (Enregistrés)

################ II- REGLAGE D'ENVIRONNEMENT

myList = os.listdir(folderPath) # importer la liste des images de barre de taches
print(myList) #afficher la liste des images
overlayList = [] #liste dans laquelle on charge la liste des images de la barre de taches
for imPath in myList:
    image = cv2.imread(f'{folderPath}/{imPath}')
    overlayList.append(image)
print(len(overlayList)) # afficher la taille de la liste des images de la barre de taches

header = overlayList[15] # donner l'image n°0 à notre header comme une image par defaut
drawColor = (0,0,255) # Couleur de choix ( Rouge par défaut )

cap = cv2.VideoCapture(0, cv2.CAP_DSHOW)  # WebCam
cap.set(3,largeurWebCam) # regler la largeur de la webCam à 1280px
cap.set(4,hauteurWebCam) # regler la hauteur de la webCam à 720px

detector = htm.handDetector(detectionCon=0.85) # importer methode de HandTrackingModule
#L'image de la WebCam ne peut pas laisser le dessin fixe à cause de son mise à jour instantané ,
# c'est pourquoi, on crée un Canvas en arrière plan qui garde l'objet dessiné.
imgCanvas = np.zeros((720,1280,3),np.uint8)

################ III- LANCEMENT D'APPLICATION

while True:

    ####Importer les images
    success, img = cap.read()
    img = cv2.flip(img, 1) #Transition gauche droite de la WebCam afin de faciliter le dessin

    ####Trouver les reperes d'une main humaine
    img = detector.findHands(img)
    lmList = detector.findPosition(img,draw=False) # charger tous les reperes des 21 points de la main humaine
    if len(lmList)!=0:
       #print(lmList)
       x1,y1 = lmList[8][1:] # les coordonnes d'index
       x2,y2 = lmList[12][1:] # les coordonnes du majeur
       ####Chercher un doigt levé
       fingers = detector.fingerUp()
       # print(fingers)


################ IV- DETERMINAISON DE MODE DE SELECTION

       ####Determinaison de geste de selection : si --l'index-- et --le majeur-- sont levés simultanément, donc on sélectionne
       if fingers[1] and fingers[2]:
          #Determinaison de type d'outil choisi pour dessiner
          if curr_tool=="Cercle" and xp>0 and yp>0:
              cv2.circle(imgCanvas, (xp, yp), int(((xp - x1) ** 2 + (yp - y1) ** 2) ** 0.5), drawColor, brushThickness)
          elif curr_tool=="Rectangle" and xp>0 and yp>0:
              cv2.rectangle(imgCanvas, (xp, yp), (x1, y1), drawColor, brushThickness)
          elif curr_tool=="Ligne" and xp>0 and yp>0:
              cv2.line(imgCanvas, (x1, y1), (xp, yp), drawColor, brushThickness)

          xp, yp = 0, 0  # une fois on est en mode de selection , on réinitialise le point de départ pour résoudre le problème de liasion entre deux dessins différents
          print("Mode de selection active")
          if y1 < Y_barreTache:  # 125px est le cordonnees d'hauteur de la barre des taches ( si on touche dans une hauteur moins que 125px, on est dans la barre des taches)

                if debutEspaceOutils < x1 < finEspaceOutils:  # si on touche dans une largeur entre 625px et 1280px on est dans l'ongle de choix d'outils
                  curr_tool = detector.getTool(x1)
                if debutEspaceCouleurs < x1 < finEspaceCouleurs and curr_tool!="Gomme":  # si on touche dans une largeur entre 9px et 552px on est dans l'ongle de choix de couleurs
                  curr_color = detector.getColor(x1)

                #On affiche l'image de barre des tâches correspondante au choix de --couleur-- et --d'outil-- correspondants
                if curr_color == "Red" and curr_tool=="Ligne":
                        print("Red Line is here")
                        header = overlayList[0]
                        drawColor = (0,0,255)
                elif curr_color == "White" and curr_tool == "Ligne":
                        print("White Line is here")
                        header = overlayList[8]
                        drawColor = (255,255,255)
                elif curr_color == "Green" and curr_tool=="Ligne":
                        print("Green Line is here")
                        header = overlayList[9]
                        drawColor = (0,255,0)
                elif curr_color == "Blue" and curr_tool=="Ligne":
                        print("Blue Line is here")
                        header = overlayList[10]
                        drawColor = (255,111,0)
                elif curr_color == "Red" and curr_tool=="Rectangle":
                        print("Red Rectangle is here");
                        header = overlayList[11]
                        drawColor = (0,0,255)
                elif curr_color == "White" and curr_tool=="Rectangle":
                        print("White Rectangle is here");
                        header = overlayList[12]
                        drawColor = (255,255,255)
                elif curr_color == "Green" and curr_tool=="Rectangle":
                        print("Green Rectangle is here");
                        header = overlayList[13]
                        drawColor = (0,255,0)
                elif curr_color == "Blue" and curr_tool=="Rectangle":
                        print("Blue Rectangle is here");
                        header = overlayList[14]
                        drawColor = (255,111,0)
                elif curr_color == "Red" and curr_tool=="Courbe":
                        print("Red Curve is here");
                        header = overlayList[15]
                        drawColor = (0,0,255)
                elif curr_color == "White" and curr_tool=="Courbe":
                        print("White Curve is here");
                        header = overlayList[16]
                        drawColor = (255,255,255)
                elif curr_color == "Green" and curr_tool=="Courbe":
                        print("Green Curve is here");
                        header = overlayList[1]
                        drawColor = (0,255,0)
                elif curr_color == "Blue" and curr_tool=="Courbe":
                        print("Blue Curve is here");
                        header = overlayList[2]
                        drawColor = (255,111,0)
                elif curr_color == "Red" and curr_tool=="Cercle":
                        print("Red Circle is here");
                        header = overlayList[3]
                        drawColor = (0,0,255)
                elif curr_color == "White" and curr_tool=="Cercle":
                        print("White Circle is here");
                        header = overlayList[4]
                        drawColor = (255,255,255)
                elif curr_color == "Green" and curr_tool=="Cercle":
                        print("Green Circle is here");
                        header = overlayList[5]
                        drawColor = (0,255,0)
                elif curr_color == "Blue" and curr_tool=="Cercle":
                        print("Blue Circle is here");
                        header = overlayList[6]
                        drawColor = (255,111,0)
                elif curr_tool == "Gomme":
                        header = overlayList[7]
                        print("Eraser is here")
                        drawColor = (0, 0, 0)

################ IV- DETERMINAISON DE MODE DE DESSIN

       ################ On determine le mode de dessin quand l'index est levé et les autres doigts ne sont pas levés
       elif fingers[1] and fingers[2]==False:
        if xp == 0 and yp == 0:  # S'il y a pas de point précédent , le point sera égal au point courant qui est le point initial
             xp, yp = x1, y1

        if curr_tool=="Ligne":  #Si l'outil de dessin est une ligne
            cv2.line(img, (xp, yp), (x1, y1), drawColor, brushThickness)

        elif curr_tool=="Rectangle":  #Choix de d#Si l'outil de dessin est un rectangle
            cv2.rectangle(img, (xp, yp), (x1,y1), drawColor,brushThickness)

        if curr_tool=="Cercle":  #Si l'outil de dessin est un cercle
            cv2.circle(img, (xp, yp), int(((xp - x1) ** 2 + (yp - y1) ** 2) ** 0.5), drawColor,brushThickness)

        elif curr_tool=="Courbe":   #Si l'outil de dessin est une courbe
           #Dessiner une ligne entre Xp,Yp : point précédent et X1,Y1 : point courant
           cv2.circle(img,(x1,y1),15, drawColor, cv2.FILLED)
           cv2.line(img, (xp,yp),(x1,y1),drawColor,brushThickness)
           cv2.line(imgCanvas, (xp,yp),(x1,y1),drawColor,brushThickness)
           xp, yp = x1, y1 #mise à jour du point précédent

        elif curr_tool=="Gomme":  #Si l'outil de dessin est une gomme
           cv2.circle(img, (x1, y1), 15, drawColor, cv2.FILLED)
           cv2.line(img, (xp, yp), (x1, y1), (0,0,0), eraserThickness)
           cv2.line(imgCanvas, (xp, yp), (x1, y1), (0,0,0), eraserThickness)
           xp, yp = x1, y1 #mise à jour du point précédent


################ V- BINARISATION DU CANVAS ( arrière plan de dessin ).

    imgGray = cv2.cvtColor(imgCanvas,cv2.COLOR_BGR2GRAY) #On convertit l'image Canvas à une image grise
    _, imgInv =  cv2.threshold(imgGray,50,255,cv2.THRESH_BINARY_INV) # Convertir l'image grise en image inversée
    imgInv = cv2.cvtColor(imgInv,cv2.COLOR_GRAY2RGB)
    img = cv2.bitwise_and(img,imgInv)
    # On ajout le OR pour permettre d'afficher les dessins en couleur dans l'img au lieu de dessin noir
    img = cv2.bitwise_or(img,imgCanvas)

    ###### préparer l'image de barre des tâches
    img[0:125,0:1280] = header

    #img = cv2.addWeighted(img,0.5,imgCanvas,0.5,0) # fusionner la WebCam et le Canvas en même image (solution moins efficaçe)

################ VI- GESTION DE LA WEBCAM.

    cv2.imshow("Image",img)  # afficher la WebCam
    #cv2.imshow("Inv",imgInv)  # afficher le Canvas ( on n'en a pas besoin )
    k = cv2.waitKey(1)
    if k % 256 == 27: # Cliquer sur ESC pour terminer la WebCam
        # ESC pressed
        print("Escape hit, closing program")
        break
    elif k % 256 == 32: # Cliquer sur ESPACE pour capturer votre dessin
        # SPACE pressed
        img_name = "Dessin_{}.png".format(Compteur_NbrCaptures)
        cv2.imwrite(img_name, img)
        print("{} written!".format(img_name))
        Compteur_NbrCaptures += 1

cap.release()
cv2.destroyAllWindows()